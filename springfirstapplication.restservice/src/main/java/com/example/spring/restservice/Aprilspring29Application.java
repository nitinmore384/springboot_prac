package com.example.spring.restservice;

import com.example.spring.restservice.di.Company;
import com.example.spring.restservice.ioc.Vodafone;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Aprilspring29Application {
	public static void main(String[] args) {
		SpringApplication.run(Aprilspring29Application.class, args);
//		ConfigurableApplicationContext context =

//		Vodafone vodafone1 = context.getBean(Vodafone.class);
//		vodafone1.calling();
//
//		Company company = context.getBean(Company.class);
//		company.display();
	}


	@GetMapping("/hii")
	public String getHiiMsg(){
		return "Hello world";
	}
}